export const state = () =>({
    staffs: [
        {
            id: 1,
            firstname: 'Matthew',
            lastname:'Roberts',
            email: "b@gmail.com",
            phone: "8827638756123",
            birthdate:null,
            joiningdate:null,
            address:'Badda',
            city:'Dhaka',
            status: "employee",
            img:require('@/assets/blank-profile-picture-973460_640.png')
        },
        {
            id: 2,
            firstname: 'Matthew',
            lastname:'Roberts',
            email: "c@gmail.com",
            phone: "8827638756123",
            birthdate:null,
            joiningdate:null,
            address:'Badda',
            city:'Dhaka',
            status: "employee",
            img:require('@/assets/blank-profile-picture-973460_640.png')
        },
    ]
});

export const getters = {
    allStaffs: (state) => state.staffs,
    getstaffById: (state) => (id) => {
        return state.staffs.find(staff => staff.id === id)
      }
};

export const actions = {};

export const mutations = {
    addStaff(state, staff) {
        state.staffs.push(staff)
    },
    updateStaff: (state, payload) => {
        const { id, firstname,lastname, email, phone,img,city,address,birthdate,joiningdate} = payload
        const staff = state.staffs.find(p => p.id === id)
        staff.firstname = firstname;
        staff.lastname = lastname;
        staff.email = email;
        staff.phone = phone;
        staff.img=img;
        staff.city=city;
        staff.address=address;
        staff.birthdate=birthdate;
        staff.joiningdate=joiningdate;
    },
    DELETE_Staff(state, id) {
        const index = state.staffs.findIndex(staff => staff.id == id)
        state.staffs.splice(index, 1)
    }
};

